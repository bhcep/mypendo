import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { DialogOverviewExampleDialogComponent } from '../dialog-overview-example-dialog/dialog-overview-example-dialog.component';

declare let pendo: any;

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email = new FormControl('');
  fullName = new FormControl('');
  account = new FormControl('');

  animal: string = '';
  name: string = '';

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  login(): void {
    if (!this.checkEmpty()) {
      console.log({ email: this.email.value.trim() });
      console.log({ account: this.account });
      // in your authentication promise handler or callback
      pendo.initialize({
        visitor: {
          id: +new Date(), // Required if user is logged in
          email: this.email.value.trim(), // Recommended if using Pendo Feedback, or NPS Email
          full_name: this.fullName.value.trim(), // Recommended if using Pendo Feedback
          role: 'Benefit Employee', // Optional

          // You can add any additional visitor level key-values here,
          // as long as it's not one of the above reserved names.
        },

        account: {
          id: this.account.value, // Highly recommended
          name: 'Bright Horizons', // Optional
          is_paying: false, // Recommended if using Pendo Feedback
          monthly_value: 4500, // Recommended if using Pendo Feedback
          planLevel: 'silver', // Optional
          planPrice: 120.0, // Optional
          // creationDate: // Optional

          // You can add any additional account level key-values here,
          // as long as it's not one of the above reserved names.
        },
      });
    }
  }

  checkEmpty(): boolean {
    return (
      this.email.value.trim() === '' ||
      this.fullName.value.trim() === '' ||
      this.account.value.trim() === ''
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialogComponent, {
      width: '90vw',
      data: { name: this.name, animal: this.animal },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
      this.animal = result;
      pendo.track('app-dialog-overview-example-dialog-ok', {
        animal: this.animal
      });
    });
  }
}
