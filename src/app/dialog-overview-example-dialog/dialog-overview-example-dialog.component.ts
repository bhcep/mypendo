import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../login/login.component';

declare let pendo: any;

@Component({
  selector: 'app-dialog-overview-example-dialog',
  templateUrl: './dialog-overview-example-dialog.component.html',
  styleUrls: ['./dialog-overview-example-dialog.component.scss']
})
export class DialogOverviewExampleDialogComponent implements OnInit {

  email11 = new FormControl('');
  email12 = new FormControl('');
  email13 = new FormControl('');
  email21 = new FormControl('');
  email22 = new FormControl('');
  email23 = new FormControl('');
  email31 = new FormControl('');
  email32 = new FormControl('');
  email33 = new FormControl('');

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {
    console.log("initialize dialog");
  }

  onNoClick(): void {
    console.log("debug app-dialog-overview-example-dialog-closed");
    pendo.track("appdialogoverviewexampledialogclosed", {
      animal: this.data.animal,
      email11: this.email11.value,
      email12: this.email12.value,
      email13: this.email13.value,
      email21: this.email21.value,
      email22: this.email22.value,
      email23: this.email23.value,
      email31: this.email31.value,
      email32: this.email32.value,
      email33: this.email33.value,
    });
    this.dialogRef.close();
  }

}
